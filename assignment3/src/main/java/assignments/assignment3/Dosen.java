package assignment3.src.main.java.assignments.assignment3;

import java.util.ArrayList;

class Dosen extends ElemenFasilkom {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah mataKuliah;

    Dosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.tipe = "Dosen";
    }

    //Digunakan untuk menentukan sapaan mahasiswa ke dosen
    public MataKuliah getMatKul() {
        return this.mataKuliah;
    }

    void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        this.mataKuliah = mataKuliah;
        mataKuliah.addDosen(this);
    }

    void dropMataKuliah() {
        /* TODO: implementasikan kode Anda di sini */
        this.mataKuliah = null;
    }
}