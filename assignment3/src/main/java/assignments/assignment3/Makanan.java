package assignment3.src.main.java.assignments.assignment3;

class Makanan {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;

    private long harga;

    Makanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.harga = harga;
    }

    public String getNama() {
        return this.nama;
    }

    public long getHarga() {
        return this.harga;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return String.format("%s", this.nama);
    }
}