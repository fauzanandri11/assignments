package assignment3.src.main.java.assignments.assignment3;

import java.util.ArrayList;

abstract class ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    protected String tipe;
    
    protected String nama;

    protected int friendship;

    //ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];
    protected ArrayList<ElemenFasilkom> telahMenyapa = new ArrayList<ElemenFasilkom>();

    String getNama() {
        return this.nama;
    }

    void menyapa(ElemenFasilkom elemenFasilkom) {
        /* TODO: implementasikan kode Anda di sini */
        boolean sudahDisapa = false;

        //mengecek apakah elemenFasilkom sudah disapa
        for (ElemenFasilkom i : telahMenyapa) {
            if (i.equals(elemenFasilkom)) {
                sudahDisapa = true;
                System.out.printf("[DITOLAK] %s telah menyapa %s hari ini", this.nama, elemenFasilkom.nama);
                return;
            } 
        }
        //Implementasi apabila mahasiswa menyapa dosen yang satu matkul
        if ((this.tipe == "Mahasiswa" && elemenFasilkom.tipe == "Dosen") && !sudahDisapa) {
            MataKuliah matKul = elemenFasilkom.getMatKul();
            
            for (Mahasiswa i : matKul.getDaftarMahasiswa()) {
                if (this.equals(i)) {
                    this.friendship += 2;
                    elemenFasilkom.friendship += 2;
                    this.telahMenyapa.add(elemenFasilkom);
                }
            }
        }
        //Implementasi menyapa biasa
        else if (!sudahDisapa) {
            System.out.printf("%s menyapa dengan %s", this.nama, elemenFasilkom.nama);
            this.friendship++;
            elemenFasilkom.friendship++;
            this.telahMenyapa.add(elemenFasilkom);
        }
    }

    void resetMenyapa() {
        /* TODO: implementasikan kode Anda di sini */
        telahMenyapa.clear();
    }

    void membeliMakanan(ElemenFasilkom pembeli, ElemenKantin penjual, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        boolean makananAda = false;
        for (Makanan i : penjual.daftarMakanan) {
            if (namaMakanan.equals(i)) {
                makananAda = true;
                System.out.printf("%s berhasil membeli %s seharga %d", pembeli.nama, namaMakanan, i.getHarga());
                pembeli.friendship++;
                penjual.friendship++;
            }
        }
        if (!makananAda) {
            System.out.printf("[DITOLAK] %s tidak menjual %s", penjual.nama, namaMakanan);
        }
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }

    // abstract method untuk implementasi mahasiswa menyapa dengan dosen yang satu matkul
    public abstract MataKuliah getMatKul();
}