package assignment3.src.main.java.assignments.assignment3;

import java.util.Scanner;
import java.util.ArrayList;

public class Main {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    //ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    private static ArrayList<ElemenFasilkom> daftarElemenFasilkom = new ArrayList<ElemenFasilkom>();

    //MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private static ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();

    public static void addMahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom.add(new Mahasiswa(nama, npm));
    }

    public static void addDosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom.add(new Dosen(nama));
    }

    public static void addElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        daftarElemenFasilkom.add(new ElemenKantin(nama));
    }

    public static void menyapa(String objek1, String objek2) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom orangPertama = null;
        ElemenFasilkom orangKedua = null;

        //mencari Objek(ElemenFasilkom) dengan nama objek1 dan objek2
        for (ElemenFasilkom i : daftarElemenFasilkom) {
            if (i.nama.equals(objek1)) {
                orangPertama = i;
            }
            else if (i.nama.equals(objek2)) {
                orangKedua = i;
            }
        }
        //cek apakah orang pertama dan kedua merupakan objek yang sama
        if (orangPertama == orangKedua) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        } else {
            orangPertama.menyapa(orangKedua);
        }
    }

    public static void addMakanan(String objek, String namaMakanan, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenKantin now = null;
        //mencari ElemenFasilkom dengan nama objek
        for (ElemenFasilkom i : daftarElemenFasilkom) {
            if (i.nama.equals(objek) && i.tipe.equals("ElemenKantin")){
                //Casting ke class ElemenKantin agar bisa call method setMakanan
                now = (ElemenKantin) i;
                now.setMakanan(namaMakanan, harga);
                break;
            } 
            //Apabila now bukan merupakan ElemenKantin
            else {
                System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin", objek);
                break;
            }
        }
    }

    public static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        ElemenFasilkom orangPertama = null;
        ElemenFasilkom orangKedua = null;

        for (ElemenFasilkom i : daftarElemenFasilkom) {
            if (i.nama.equals(objek1)) {
                orangPertama = i;
            }
            else if (i.nama.equals(objek2)) {
                orangKedua = i;
            }
        }
        //Cek apakah orangKedua merupakan ElemenKantin
        if (orangKedua.tipe.equals("ElemenKantin")) {
            //Cek apabila orangPertama dan orangKedua merupakan objek yang sama
            if (orangPertama == orangKedua) {
                System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
            }
            //Apabila semua persyaratan terpenuhi
            else {
                ElemenKantin orangKedua2 = (ElemenKantin) orangKedua;
                orangPertama.membeliMakanan(orangPertama, orangKedua2, namaMakanan);
            }
        }
        //Apabila orangKedua bukan ElemenaKantin
        else {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        }
    }

    public static void createMatkul(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        daftarMataKuliah.add(new MataKuliah(nama, kapasitas));
    }

    public static void addMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        Mahasiswa now = null;
        MataKuliah matkul = null;
        boolean tipeCocok = false;

        //Mancari objek MataKuliah yang bernama namaMataKuliah
        for (MataKuliah j : daftarMataKuliah) {
            if (j.getNama().equals(namaMataKuliah)) {
                matkul = j;
            }
        }
        //Mencari Objek yang bernama objek
        for (ElemenFasilkom i : daftarElemenFasilkom) {
            if (i.nama.equals(objek) && i.tipe.equals("Mahasiswa")) {
                tipeCocok = true;
                now = (Mahasiswa) i;
                now.addMatkul(matkul);
                break;
            } else {
                System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
                break;
            }
        }
    }

    public static void dropMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        Mahasiswa mahasiswa = null;
        MataKuliah matkul = null;

        for (MataKuliah j : daftarMataKuliah) {
            if (j.equals(namaMataKuliah)) {
                matkul = j;
                break;
            }
        }
        for (ElemenFasilkom i : daftarElemenFasilkom) {
            if (i.nama.equals(objek)) {
                mahasiswa = (Mahasiswa) i;
                break;
            }
        }
        if (mahasiswa.tipe.equals("Mahasiswa")) {
            mahasiswa.dropMatkul(matkul);
        } else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        }
    }

    public static void mengajarMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        Dosen dosen = null;
        MataKuliah matkul = null;

        for (MataKuliah j : daftarMataKuliah) {
            if (j.equals(namaMataKuliah)) {
                matkul = j;
                break;
            }
        }
        for (ElemenFasilkom i : daftarElemenFasilkom) {
            if (i.nama.equals(objek)) {
                dosen = (Dosen) i;
                break;
            }
        }
        if (dosen.tipe.equals("Dosen")) {
            dosen.mengajarMataKuliah(matkul);
            matkul.addDosen(dosen);
        } else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        }
    }

    public static void berhentiMengajar(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        Dosen dosen = null;
        MataKuliah matkul = null;
        
        for (ElemenFasilkom i : daftarElemenFasilkom) {
            if (i.nama.equals(objek)) {
                dosen = (Dosen) i;
                break;
            }
        }
        if (dosen.tipe.equals("Dosen")) {
            matkul = dosen.getMatKul(); 
            matkul.dropDosen();
            dosen.dropMataKuliah();
        } else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        }
        
    }

    public static void ringkasanMahasiswa(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        Mahasiswa mahasiswa = null;

        //Mencari objek Mahasiswa yang ingin diprint ringkasannya
        for (ElemenFasilkom i : daftarElemenFasilkom) {
            if (i.nama.equals(objek)) {
                mahasiswa = (Mahasiswa) i;
                break;
            }
        }
        //Mengecek tipe objek mahasiswa
        if (mahasiswa.tipe.equals("Mahasiswa")) {
            System.out.printf("Nama: %s", mahasiswa);
            System.out.printf("Tanggal lahir: %s", mahasiswa.getTanggalLahir());
            System.out.printf("Jurusan: %s", mahasiswa.getJurusan());
            mahasiswa.getListMataKuliah();
        } else {
            System.out.printf("[DITOLAK] [nama_objek] bukan merupakan seorang mahasiswa", objek);
        }
    }

    public static void ringkasanMataKuliah(String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        MataKuliah matkul = null;
        //Mencari objek MataKuliah yang ingin diprint ringkasannya
        for (MataKuliah i : daftarMataKuliah) {
            if (i.getNama().equals(namaMataKuliah)) {
                matkul = i;
            }
        }
        System.out.printf("Nama mata kuliah: %s", matkul.getNama());
        System.out.printf("Jumlah mahasiswa: %d", matkul.getJmlMahasiswa());
        System.out.printf("Kapasitas: %d", matkul.getKapasitas());
        System.out.print("Dosen pengajar: ");
        //Mengecek apakah ada dosen pengajar
        if (matkul.getDosen().equals(null)) {
            System.out.println("Belum ada");
        } else {
            System.out.printf("%s", matkul.getDosen().nama);
        }
        System.out.print("Daftar mahasiswa yang mengambil mata kuliah ini: ");
        matkul.getListMatakuliah();
    }

    public static void nextDay() {
        /* TODO: implementasikan kode Anda di sini */
        //Looping untuk semua Objek ElemenFasilkom
        for (ElemenFasilkom i : daftarElemenFasilkom) {
            boolean syarat1 = i.friendship >= 0;
            boolean syarat2 = i.friendship <= 100;
            boolean syarat3 = i.telahMenyapa.size() >= ((daftarElemenFasilkom.size() - 1) / 2); 
            //Menambahkan atau mengurangi friendship berdasarkan syarat
            if (syarat3) {
                i.friendship += 10;
                if (!syarat2) {
                    i.friendship = 100;
                } 
            } else {
                i.friendship -= 5;
                if (!syarat1) {
                    i.friendship = 0;
                }
            }
            //Reset arraylist telahMenyapa
            i.resetMenyapa();
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    public static void friendshipRanking() {
        /* TODO: implementasikan kode Anda di sini */
        ArrayList<ElemenFasilkom> ranking = new ArrayList<ElemenFasilkom>();
        int maxFriendship = 0;
        int indexMax = 0;
        //Copying arraylist
        for (ElemenFasilkom i : daftarElemenFasilkom) {
            ranking.add(i);
        }

        //Mencari friendship terbesar dari ElemenFasilkom(yang sudah dicopy ke variabel ranking),
        //kemudian di print nama dan nilai friendshipnya dan dihapus dari variabel
        //ranking (agar tidak di print 2 kali)
        for (int i = 0; i < ranking.size(); i++) {
            for (int j = 0; j < ranking.size(); j++) {
                if (ranking.get(i).friendship > maxFriendship) {
                    maxFriendship = ranking.get(i).friendship;
                    indexMax = j;
                }
            }
            System.out.print(i+1);
            System.out.printf(" %s(%d)", ranking.get(indexMax).nama, ranking.get(indexMax).friendship);
            ranking.remove(indexMax);
        }
    }

    public static void programEnd() {
        /* TODO: implementasikan kode Anda di sini */
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada fasilkom");
        friendshipRanking();
        System.exit(0);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}