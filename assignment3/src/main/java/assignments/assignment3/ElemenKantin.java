package assignment3.src.main.java.assignments.assignment3;

import java.util.ArrayList;

class ElemenKantin extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    //private Makanan[] daftarMakanan = new Makanan[10];
    ArrayList<Makanan> daftarMakanan = new ArrayList<Makanan>();

    public ElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.tipe = "ElemenKantin";
    }

    public MataKuliah getMatKul() {
        return null;
    }
    public String getTipe() {
        return this.tipe;
    }

    public 
    void setMakanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        boolean sudahAda = false;
        for (Makanan i : daftarMakanan) {
            if (i.getNama().equals(nama)) {
                sudahAda = true;
                System.out.printf("[DITOLAK] %s sudah pernah terdaftar", nama);
            }
        }
        if (!sudahAda) {
            this.daftarMakanan.add(new Makanan(nama, harga));
            System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d", this.nama, nama, harga);
        }
    }
}