package assignment3.src.main.java.assignments.assignment3;

import java.util.ArrayList;

class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    //Mahasiswa[] daftarMahasiswa;
    private ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<Mahasiswa>();

    MataKuliah(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.dosen = null;
    }

    public void getListMatakuliah() {
        if (daftarMahasiswa.size() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else{
            for (int i = 0; i < daftarMahasiswa.size(); i++) {
                System.out.print(i+1);
                System.out.println(daftarMahasiswa.get(i));
            }
        }
    }

    public String getNama() {
        return this.nama;
    }

    public ArrayList<Mahasiswa> getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    public int getKapasitas() {
        return this.kapasitas;
    }

    public int getJmlMahasiswa() {
        return this.daftarMahasiswa.size();
    }

    public Dosen getDosen() {
        return this.dosen;
    }

    void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        daftarMahasiswa.add(mahasiswa);
    }

    void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        for (Mahasiswa i : daftarMahasiswa) {
            if (i.equals(mahasiswa)) {
                daftarMahasiswa.remove(i);
            }
        }
    }

    void addDosen(Dosen dosen) {
        /* TODO: implementasikan kode Anda di sini */
        this.dosen = dosen;
    }

    void dropDosen() {
        /* TODO: implementasikan kode Anda di sini */
        this.dosen = null;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return this.nama;
    }
}