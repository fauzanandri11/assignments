package assignment3.src.main.java.assignments.assignment3;

import java.util.ArrayList;

class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    //MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    Mahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.npm = npm;
        this.tipe = "Mahasiswa";
        this.jurusan = extractJurusan(npm);
        this.tanggalLahir = extractTanggalLahir(npm);
    }

    public String getTanggalLahir() {
        return this.tanggalLahir;
    }

    public String getJurusan() {
        return this.jurusan;
    }

    public MataKuliah getMatKul() {
        return null;
    }

    // Print mata kuliah yang sudah diambil oleh Objek mahasiswa
    public void getListMataKuliah() {
        if (daftarMataKuliah.size() == 0) {
            System.out.println("Belum ada mata kuliah yang diambil");
        } else {
            System.out.println("Daftar mata kuliah:");
            for (int i = 0; i < daftarMataKuliah.size(); i++) {
                System.out.print(i+1);
                System.out.println(" " + daftarMataKuliah.get(i));
            }
        }
    }

    public void addMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        boolean sudahDiambil = false;
        boolean kapasitasPenuh = false;
        String nama = mataKuliah.getNama();
        for (MataKuliah i : daftarMataKuliah) {
            if (i.equals(mataKuliah)) {
                sudahDiambil = true;
                System.out.printf("[DITOLAK] %s telah diambil sebelumnya", nama);
            }
        }
        if (mataKuliah.getJmlMahasiswa() >= mataKuliah.getKapasitas()) {
            kapasitasPenuh = true;
            System.out.printf("[DITOLAK] %s telah penuh kapasitasnya", nama);
        }
        if (!sudahDiambil && !kapasitasPenuh) {
            this.daftarMataKuliah.add(mataKuliah);
            mataKuliah.addMahasiswa(this);
            System.out.printf("%s berhasil menambahkan mata kuliah %s", this.nama, nama);
        }
    }

    void dropMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        for (MataKuliah i : daftarMataKuliah) {
            if (mataKuliah.equals(i)) {
                daftarMataKuliah.remove(i);
                mataKuliah.dropMahasiswa(this);
                System.out.printf("%s berhasil drop mata kuliah %s", this.nama, mataKuliah.getNama());
            }
        }
    }

    String extractTanggalLahir(long npm) {
        /* TODO: implementasikan kode Anda di sini */
        String a = Long.toString(npm).substring(4, 12);
        String tanggal = a.substring(0, 2) + "-" + a.substring(2, 4) + "-" + a.substring(4, 8); 
        return tanggal;
    }

    public String extractJurusan(long npm) {
        /* TODO: implementasikan kode Anda di sini */
        int a = Integer.parseInt(Long.toString(npm).substring(2, 4));
        String res = "";
        switch (a) {
            case 1 :
            res = "Ilmu Komputer";
            break;

            case 2 :
            res = "Sistem Informasi";
            break;
        }
        return res;
    }
}