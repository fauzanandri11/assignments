package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */

    // Method Validasi NPM
    public static boolean validate(long num) {
        String npm = Long.toString(num);
        int sum = 0;

        // Cek syarat pertama yaitu panjang NPM harus 14 digit
        if (npm.length() == 14){
            String A = npm.substring(0, 2);
            int B = Integer.valueOf(npm.substring(2, 4));
            String C = npm.substring(8, 12);
            char E = npm.charAt(13);

            //Cek syarat kedua yaitu kode jurusan harus valid
            if ( (B>=1 && B<=3) || (B>=11 && B<=12) ){

                //Cek syarat ketiga yaitu umur pada npm lebih dari 15 tahun
                if (Short.valueOf("20" + A) - Short.valueOf(C) >= 15){

                    //Cek syarat keempat yaitu digit terakhir NPM sesuai ketentuan penghitungan
                    for (int i = 0; i < 6; i++){
                        sum += Character.getNumericValue(npm.charAt(i)) * Character.getNumericValue(npm.charAt(npm.length() - (i+2)));
                    }
                    while (sum > 9){
                        sum =  sum/10 + sum%10;
                    }
                    if (Character.getNumericValue(E) == sum){
                        return true;
                    }
                }
            } 
        }
        // Return false apabila ada syarat yang tidak terpenuhi
        return false;
    }

    //Method ekstrak informasi dari NPM
    public static String extract(long num){
        String npm = Long.toString(num);
        String A = npm.substring(0, 2);
        int B = Integer.valueOf(npm.substring(2, 4));
        String jurusan;
        String res = "";
        
        // if conditional untuk menentukan jurusan mahasiswa
        if (B == 1){
            jurusan = "Ilmu Komputer";
        }
        else if (B == 2){
            jurusan = "Sistem Informasi";
        }
        else if (B == 3){
            jurusan = "Teknologi Informasi";
        }
        else if (B == 4){
            jurusan = "Teknik Telekomunikasi";
        }
        else{
            jurusan = "Teknik Elektro";
        }

        // concate informasi mahasiswa ke variabel res
        // yang sudah di deklarasi sebelumnya
        res += "Tahun masuk: 20" + A + "\n";
        res += "Jurusan: " + jurusan + "\n";
        res += "Tanggal Lahir: " + npm.substring(4,6) + "-" + npm.substring(6,8) + "-" + npm.substring(8,12);
        return res;
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        System.out.print("Masukkan NPM : ");
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            // TODO: Check validate and extract NPM
        
            //Menjalankan method validate() kemudian menjalankan
            //method extract() jika method validate() mereturn true
            if (validate(npm) == true){
                System.out.println(extract(npm));
            }
            // Print "NPM tidak valid" apabila memthod validate() mereturn false
            else{
                System.out.println("NPM tidak valid!");
            }
        }
        input.close();

        /*
        Notes :
        1. Jangan gunakan nested if-else, buat 
        */
    }
}
