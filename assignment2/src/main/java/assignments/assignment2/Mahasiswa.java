package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    private int jmlmatkul;
    private int jmlMasalah;

    public Mahasiswa(String nama, long npm){
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.npm = npm;
        this.jurusan = extractjurusan(this.npm);
        this.masalahIRS = new String[20];
    }
    
    public long getNpm() {
        return this.npm;
    }

    public String getJurusan() {
        return this.jurusan;
    }

    public int getTotalSKS() {
        return this.totalSKS;
    }

    public int getJmlMatkul() {
        return this.jmlmatkul;
    }

    public MataKuliah[] getMatKul() {
        return this.mataKuliah;
    }

    public String[] getMasalahIRS() {
        return this.masalahIRS;
    }

    public int getJmlMasalah() {
        return this.jmlMasalah;
    }
    
    public void addMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        this.mataKuliah[this.jmlmatkul] = mataKuliah;
        this.jmlmatkul++;
        this.totalSKS += mataKuliah.getSks();
    }

    public void dropMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < jmlmatkul; i++) {
            if (this.mataKuliah[i].getNama().equals(mataKuliah.getNama())) {
                jmlmatkul--;
                this.mataKuliah[i] = this.mataKuliah[jmlmatkul];
                this.mataKuliah[jmlmatkul] = null;
                this.totalSKS -= mataKuliah.getSks();
            }
        }
    }

    public void cekIRS(){
        /* TODO: implementasikan kode Anda di sini */
        this.jmlMasalah = 0;
        if (this.totalSKS > 24) {
            this.masalahIRS[this.jmlMasalah] = "SKS yang Anda ambil lebih dari 24";
            this.jmlMasalah++;
        }
        String singkatanJurusan = "";
        if (this.jurusan.equals("Ilmu Komputer")) {
            singkatanJurusan = "IK";
        }
        else {
            singkatanJurusan = "SI";
        }
        for (int i = 0; i < this.jmlmatkul; i++) {
            if (!this.mataKuliah[i].getKode().equals(singkatanJurusan)) {
                this.masalahIRS[this.jmlMasalah] = String.format("Mata Kuliah %s tidak dapat diambil jurusan %s", this.mataKuliah[i].getNama(), singkatanJurusan);
                this.jmlMasalah++;
            }
        }
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return String.format("%s", this.nama);
    }

    public static String extractjurusan(long npm) {
        String jurusan = "";
        String str = Long.toString(npm);
        int subjurusan = Integer.parseInt(str.substring(2, 4));

        switch (subjurusan) {
            case 1:  
                jurusan = "Ilmu Komputer";
                break;
            case 2:  
                jurusan = "Sistem Informasi";
                break;
        }
        return jurusan;
    }
}
