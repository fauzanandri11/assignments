package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;

    private int jmlmahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        /* TODO: implementasikan kode Anda di sini */
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
    }

    public int getSks() {
        return this.sks;
    }

    public String getKode() {
        return this.kode;
    }

    public String getNama() {
        return this.nama;
    }

    public int  getKapasitas() {
        return this.kapasitas;
    }

    public int getJmlMahasiswa() {
        return this.jmlmahasiswa;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        this.daftarMahasiswa[jmlmahasiswa] = mahasiswa;
        this.jmlmahasiswa++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < jmlmahasiswa; i++) {
            if (daftarMahasiswa[i] == mahasiswa) {
                daftarMahasiswa[i] = daftarMahasiswa[i+1];
                daftarMahasiswa[i+1] = null;
            }
        }
        this.jmlmahasiswa--;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return String.format("%s", this.nama);
    }
}
