package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{
    private JFrame frame;
    private JButton tambahkanButton = new JButton("Tambahkan");
    private JButton kembaliButton = new JButton("Kembali");

    private JLabel kodeLbl = new JLabel("Kode Mata Kuliah:");
    private JLabel namaLbl = new JLabel("Nama Mata Kuliah:");
    private JLabel sksLbl = new JLabel("SKS:");
    private JLabel kapasitasLbl = new JLabel("Kapasitas:");

    private JTextField kodeField = new JTextField(12);
    private JTextField namaField = new JTextField(12);
    private JTextField sksField = new JTextField(12);
    private JTextField kapasitasField = new JTextField(12);

    private JPanel tambahMatkulPanel = new JPanel(new GridLayout(11, 1, 10, 10));

    private ArrayList<Mahasiswa> daftarMahasiswa;
    private ArrayList<MataKuliah> daftarMataKuliah;

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // TODO: Implementasikan Tambah Mata Kuliah
        this.frame = frame;
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        tambahkanButton.addActionListener(new tambahkanListener());
        kembaliButton.addActionListener(new kembaliListener());
        
        // TODO: Implementasikan Halaman Home
        frame.setLayout(null);

        kodeLbl.setHorizontalAlignment(JLabel.CENTER);
        namaLbl.setHorizontalAlignment(JLabel.CENTER);
        sksLbl.setHorizontalAlignment(JLabel.CENTER);
        kapasitasLbl.setHorizontalAlignment(JLabel.CENTER);

        tambahMatkulPanel.setBounds(0, 50, 500, 400);
        tambahMatkulPanel.add(titleLabel);
        tambahMatkulPanel.add(kodeLbl);
        tambahMatkulPanel.add(kodeField);
        tambahMatkulPanel.add(namaLbl);
        tambahMatkulPanel.add(namaField);
        tambahMatkulPanel.add(sksLbl);
        tambahMatkulPanel.add(sksField);
        tambahMatkulPanel.add(kapasitasLbl);
        tambahMatkulPanel.add(kapasitasField);
        tambahMatkulPanel.add(tambahkanButton);
        tambahMatkulPanel.add(kembaliButton);
        

        frame.add(tambahMatkulPanel);

    }

    class tambahkanListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String kode = kodeField.getText();
            String nama = namaField.getText();
            String sksStr = sksField.getText();
            String kapasitasStr = kapasitasField.getText();

            boolean sudahAda = false;
            boolean fieldKosong = true;
            String str = "";
            int sks = 0;
            int kapasitas = 0;

            //mengganti flag fieldKosong apabila da field yang kosong
            if (! (kode.equals("") || nama.equals("") || sksStr.equals("") || kapasitasStr.equals(""))) {
                fieldKosong = false;
                sks = Integer.parseInt(sksStr);
                kapasitas = Integer.parseInt(kapasitasStr);
            }

            //Cek apakah nama mata kuliah yang ingin di add sudah ada
            for (int i = 0; i < daftarMataKuliah.size(); i++) {
                if (daftarMataKuliah.get(i).getNama().equals(nama)) {
                    sudahAda = true;
                    break;
                }
            }

            //dijalankan apabila semua flag false
            if (!sudahAda && !fieldKosong) {
                matkulAddSort(new MataKuliah(kode, nama, sks, kapasitas));
                kodeField.setText("");
                namaField.setText("");
                sksField.setText("");
                kapasitasField.setText("");

                str = String.format("Mata Kuliah %s berhasil ditambahkan", nama);
                new OptionPane(str);
            } else if (fieldKosong){ //dijalankan bila ada field kosong
                new OptionPane("Mohon isi seluruh field");
            } else { // dijalankan bila nama mata kuliah sudah ada
                str = String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", nama);
                new OptionPane(str);
            }
        }
    }

    class kembaliListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();
            
            new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();
        }
    }

    //Sorting dilakukan tiap kali add object MataKuliah baru dengan cara
    //membandingkan nama mata kuliah yang baru di add dengan nama mata
    //kuliah yang sudah ada
    public void matkulAddSort(MataKuliah e) {
        String curr = e.getNama();
        if (daftarMataKuliah.size() > 0){
            for (int i = 0; i < daftarMataKuliah.size(); i++) {
                if (daftarMataKuliah.get(i).getNama().compareTo(curr) > 0) {
                    daftarMataKuliah.add(i, e);
                    return;
                } else if (i == (daftarMataKuliah.size()-1)){
                    daftarMataKuliah.add(e);
                    return;
                }
            }
        } else {
            daftarMataKuliah.add(e);
        }
    }

    class OptionPane {  
        JFrame f;  
        OptionPane(String str){  
            f = new JFrame();  
            JOptionPane.showMessageDialog(f, str);  
        }  
    }
    
}
