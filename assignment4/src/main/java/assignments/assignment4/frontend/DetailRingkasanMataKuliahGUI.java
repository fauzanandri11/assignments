package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {

    private JFrame frame;

    private JLabel titleLabel = new JLabel("Detail Ringkasan Mata Kuliah");
    private JLabel namaLabel = new JLabel();
    private JLabel kodeLabel = new JLabel();
    private JLabel sksLabel = new JLabel();
    private JLabel jumlahMahasiswaLbl = new JLabel();
    private JLabel kapasitasLabel = new JLabel();
    private JLabel daftarMahasiswaLbl = new JLabel("Daftar Mahasiswa:");
    
    private JButton selesaiButton = new JButton("Selesai");

    private JPanel homePanel = new JPanel(new GridLayout(14, 1, 5, 5));

    private ArrayList<Mahasiswa> daftarMahasiswa;
    private ArrayList<MataKuliah> daftarMataKuliah;

    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Detail Ringkasan Mata Kuliah
        this.frame = frame;
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;
        
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        namaLabel.setText(String.format("Nama mata kuliah: %s", mataKuliah.getNama()));
        namaLabel.setHorizontalAlignment(JLabel.CENTER);
        kodeLabel.setText(String.format("Kode: %s", mataKuliah.getKode()));
        kodeLabel.setHorizontalAlignment(JLabel.CENTER);
        sksLabel.setText(String.format("SKS: %d", mataKuliah.getSKS()));
        sksLabel.setHorizontalAlignment(JLabel.CENTER);
        jumlahMahasiswaLbl.setText(String.format("Jumlah mahasiswa: %d", mataKuliah.getJumlahMahasiswa()));
        jumlahMahasiswaLbl.setHorizontalAlignment(JLabel.CENTER);
        kapasitasLabel.setText(String.format("Kapasitas: %d", mataKuliah.getKapasitas()));
        kapasitasLabel.setHorizontalAlignment(JLabel.CENTER);
        daftarMahasiswaLbl.setHorizontalAlignment(JLabel.CENTER);

        frame.setLayout(null);
        
        selesaiButton.addActionListener(new selesaiListener());
        
        homePanel.setBounds(0, 50, 500, 500);
        homePanel.add(titleLabel);
        homePanel.add(namaLabel);
        homePanel.add(kodeLabel);
        homePanel.add(sksLabel);
        homePanel.add(jumlahMahasiswaLbl);
        homePanel.add(kapasitasLabel);
        homePanel.add(daftarMahasiswaLbl);
        printMahasiswa(mataKuliah);
        homePanel.add(selesaiButton);
        homePanel.setAutoscrolls(true);

        frame.getContentPane().add(homePanel);
    }

    class selesaiListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();

            new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            frame.validate();
        }
    }

    public void printMahasiswa(MataKuliah mataKuliah) {
        String str = "";
        if (mataKuliah.getJumlahMahasiswa() > 0) {
            for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++) {
                str = String.format("%d. %s", i+1, mataKuliah.getDaftarMahasiswa()[i].getNama());
                homePanel.add(new JLabel(str, JLabel.CENTER));
            }
        } else {
            homePanel.add(new JLabel("Belum ada mahasiswa yang mengambil mata kuliah ini.", JLabel.CENTER));
        }
    }
}
