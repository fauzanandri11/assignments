package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;


import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI {
    private JFrame frame;
    private JButton tambahkanButton = new JButton("Tambahkan");
    private JButton kembaliButton = new JButton("Kembali");

    private JLabel namaLbl = new JLabel("Nama:");
    private JLabel npmLbl = new JLabel("NPM:");

    private JTextField namaField = new JTextField(12);
    private JTextField npmField = new JTextField(12);

    private JPanel tambahMahasiswaPanel = new JPanel(new GridLayout(7, 1, 10, 10));

    private ArrayList<Mahasiswa> daftarMahasiswa;
    private ArrayList<MataKuliah> daftarMataKuliah;

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        this.frame = frame;
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        namaLbl.setHorizontalAlignment(JLabel.CENTER);
        npmLbl.setHorizontalAlignment(JLabel.CENTER);

        tambahkanButton.addActionListener(new tambahkanListener());
        kembaliButton.addActionListener(new kembaliListener());
        
        // TODO: Implementasikan Halaman Home
        frame.setLayout(null);

        tambahMahasiswaPanel.setBounds(0, 50, 500, 300);
        tambahMahasiswaPanel.add(titleLabel);
        tambahMahasiswaPanel.add(namaLbl, BorderLayout.CENTER);
        tambahMahasiswaPanel.add(namaField, BorderLayout.CENTER);
        tambahMahasiswaPanel.add(npmLbl, BorderLayout.CENTER);
        tambahMahasiswaPanel.add(npmField, BorderLayout.CENTER);
        tambahMahasiswaPanel.add(tambahkanButton);
        tambahMahasiswaPanel.add(kembaliButton);

        frame.add(tambahMahasiswaPanel);

    }

    class tambahkanListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String nama = namaField.getText();
            String npmString = npmField.getText();
            boolean sudahAda = false;
            boolean fieldKosong = true;
            String str = "";
            long npm = 0;

            //mengganti flag fieldKosong apabila da field yang kosong
            if (! (nama.equals("") || npmString.equals(""))) {
                fieldKosong = false;
                npm = Long.parseLong(npmString);
            }

            //Cek apakah npm mahasiswa yang ingin di add sudah ada
            for (Mahasiswa i : daftarMahasiswa) {
                if (i.getNpm() == npm) {
                    sudahAda = true;
                }
            }
            //dijalankan apabila semua flag false
            if (!sudahAda && !fieldKosong) {
                mahasiswaAddSort(new Mahasiswa(nama, npm));
                namaField.setText("");
                npmField.setText("");

                str = String.format("Mahasiswa %d %s berhasil ditambahkan", npm, nama);
                new OptionPane(str);
            } else if (fieldKosong){ //dijalankan bila ada field kosong
                new OptionPane("Mohon isi seluruh field");
            } else { // dijalankan bila npm mahasiswa sudah ada
                str = String.format("NPM %d sudah pernah ditambahkan sebelumnya", npm);
                new OptionPane(str);
            }
        }
    }

    class kembaliListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
    
            frame.getContentPane().removeAll();
            frame.repaint();
            
            new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();        
        }
    }

    // Sorting dilakukan dengan menempatkan object mahasiswa yang baru dimasukkan
    // ke arraylist daftarMahasiswa berdasarkan perbandingan nilai npm-nya dari
    // object mahasiswa yang sudah ada
    public void mahasiswaAddSort(Mahasiswa e) {
        long curr = e.getNpm();
        if (daftarMahasiswa.size() > 0){
            for (int i = 0; i < daftarMahasiswa.size(); i++) {
                if (daftarMahasiswa.get(i).getNpm() > curr) {
                    daftarMahasiswa.add(i, e);
                    return;
                } else if (i == (daftarMahasiswa.size()-1)){
                    daftarMahasiswa.add(e);
                    return;
                }
            }
        } else {
            daftarMahasiswa.add(e);
        }
    }

    class OptionPane {  
        JFrame f;  
        OptionPane(String str){  
            f = new JFrame();  
            JOptionPane.showMessageDialog(f, str);  
        }  
    }
}

