package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Objects;

import assignments.assignment4.backend.*;

public class TambahIRSGUI {
    private JFrame frame;
    private JButton tambahkanButton = new JButton("Tambahkan");
    private JButton kembaliButton = new JButton("Kembali");

    private JLabel npmLbl = new JLabel("Pilih NPM:");
    private JLabel matkulLbl = new JLabel("Pilih Nama Matkul:");

    private JComboBox<Long> npmBox;
    private JComboBox<String> matkulBox;

    private JPanel tambahMahasiswaPanel = new JPanel(new GridLayout(7, 1, 10, 10));

    private ArrayList<Mahasiswa> daftarMahasiswa;
    private ArrayList<MataKuliah> daftarMataKuliah;

    private Long[] listNPM;
    private String[] listMatkul;

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Tambah IRS
        this.frame = frame;
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah IRS");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        listNPM = npmLister(daftarMahasiswa);
        listMatkul = matkulLister(daftarMataKuliah);

        npmBox = new JComboBox<Long>(listNPM);
        matkulBox = new JComboBox<String>(listMatkul);

        npmLbl.setHorizontalAlignment(JLabel.CENTER);
        matkulLbl.setHorizontalAlignment(JLabel.CENTER);

        
        tambahkanButton.addActionListener(new tambahkanListener());
        kembaliButton.addActionListener(new kembaliListener());
        
        frame.setLayout(null);

        tambahMahasiswaPanel.setBounds(0, 50, 500, 300);
        tambahMahasiswaPanel.add(titleLabel);
        tambahMahasiswaPanel.add(npmLbl);
        tambahMahasiswaPanel.add(npmBox);
        tambahMahasiswaPanel.add(matkulLbl);
        tambahMahasiswaPanel.add(matkulBox);
        tambahMahasiswaPanel.add(tambahkanButton);
        tambahMahasiswaPanel.add(kembaliButton);
        

        frame.add(tambahMahasiswaPanel); 
        
    }

    class tambahkanListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String npmStr = "";
            String matkul = "";
            long npm = 0;
            Mahasiswa mahasiswa = null;
            MataKuliah mataKuliah = null;
            String str = "";

            boolean fieldKosong = true;

            
            if (! (Objects.isNull(matkulBox.getSelectedItem()) || Objects.isNull(npmBox.getSelectedItem()))) {
                fieldKosong = false;
                npmStr = String.valueOf(npmBox.getSelectedItem());
                matkul = String.valueOf(matkulBox.getSelectedItem());
                npm = Long.parseLong(npmStr);
                mahasiswa = getMahasiswa(npm);
                mataKuliah = getMataKuliah(matkul);
            }

            if (!fieldKosong) {
                str = mahasiswa.addMatkul(mataKuliah);
                
                new OptionPane(str);
            } else {
                new OptionPane("Mohon isi seluruh field");
            }
        }
    }

    class kembaliListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();
            
            new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();
        }
    }

    public Long[] npmLister(ArrayList<Mahasiswa> e) {
        Long[] listNPM = new Long[e.size()];
        for (int i = 0; i < e.size(); i++) {
            listNPM[i] = e.get(i).getNpm();
        }

        return listNPM;
    }

    public String[] matkulLister(ArrayList<MataKuliah> e) {
        String[] listMatkul = new String[e.size()];

        for (int i = 0; i < e.size(); i++) {
            listMatkul[i] = e.get(i).getNama();
        }

        return listMatkul;
    }

    class OptionPane {  
        JFrame f;  
        OptionPane(String str){  
            f = new JFrame();  
            JOptionPane.showMessageDialog(f, str);  
        }  
    }
    // Uncomment method di bawah jika diperlukan
    
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
    
}
