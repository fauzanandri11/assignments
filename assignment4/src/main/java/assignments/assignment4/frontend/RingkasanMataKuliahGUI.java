package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Objects;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    private JFrame frame;
    private JButton lihatButton = new JButton("Lihat");
    private JButton kembaliButton = new JButton("Kembali");

    private JLabel namaMatkulLbl = new JLabel("Pilih Nama Matkul");

    private JComboBox<String> namaMatkulBox = new JComboBox<String>();

    private JPanel ringkasanMatkulPanel = new JPanel(new GridLayout(5, 1, 10, 10));

    private ArrayList<Mahasiswa> daftarMahasiswa;
    private ArrayList<MataKuliah> daftarMataKuliah;

    private String[] listMatkul;

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Ringkasan Mata Kuliah
        this.frame = frame;
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        listMatkul = matkulLister(daftarMataKuliah);
        namaMatkulBox = new JComboBox<String>(listMatkul);

        namaMatkulLbl.setHorizontalAlignment(JLabel.CENTER);

        lihatButton.addActionListener(new lihatListener());
        kembaliButton.addActionListener(new kembaliListener());
        
        frame.setLayout(null);

        ringkasanMatkulPanel.setBounds(0, 50, 500, 300);
        ringkasanMatkulPanel.add(titleLabel);
        ringkasanMatkulPanel.add(namaMatkulLbl);
        ringkasanMatkulPanel.add(namaMatkulBox);
        ringkasanMatkulPanel.add(lihatButton);
        ringkasanMatkulPanel.add(kembaliButton);
        

        frame.add(ringkasanMatkulPanel); 
        
    }

    class lihatListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String namaStr = "";
            MataKuliah mataKuliah = null;
            boolean fieldKosong = true;

            if (! Objects.isNull(namaMatkulBox.getSelectedItem())) {
                fieldKosong = false;
                namaStr = String.valueOf(namaMatkulBox.getSelectedItem());
                mataKuliah = getMataKuliah(namaStr);
            }

            if (!fieldKosong) {
                frame.getContentPane().removeAll();
                frame.repaint();

                new DetailRingkasanMataKuliahGUI(frame, mataKuliah, daftarMahasiswa, daftarMataKuliah);
                frame.validate();

            } else {
                new OptionPane("Mohon isi seluruh field");
            }

        }
    }

    class kembaliListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();
            
            new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();
        }
    }

    public String[] matkulLister(ArrayList<MataKuliah> e) {
        String[] listMatkul = new String[e.size()];

        for (int i = 0; i < e.size(); i++) {
            listMatkul[i] = e.get(i).getNama();
        }

        return listMatkul;
    }

    class OptionPane {  
        JFrame f;  
        OptionPane(String str){  
            f = new JFrame();  
            JOptionPane.showMessageDialog(f, str);  
        }  
    }

    // Uncomment method di bawah jika diperlukan
    
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
    
}
