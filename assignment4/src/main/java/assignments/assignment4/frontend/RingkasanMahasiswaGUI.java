package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Objects;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    private JFrame frame;
    private JButton lihatButton = new JButton("Lihat");
    private JButton kembaliButton = new JButton("Kembali");

    private JLabel npmLbl = new JLabel("Pilih NPM");

    private JComboBox<Long> npmBox;

    private JPanel ringkasanMahasiswaPanel = new JPanel(new GridLayout(5, 1, 10, 10));

    private ArrayList<Mahasiswa> daftarMahasiswa;
    private ArrayList<MataKuliah> daftarMataKuliah;

    private Long[] listNPM;

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Ringkasan Mahasiswa
        this.frame = frame;
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        listNPM = npmLister(daftarMahasiswa);
        npmBox = new JComboBox<Long>(listNPM);

        npmLbl.setHorizontalAlignment(JLabel.CENTER);

        lihatButton.addActionListener(new lihatListener());
        kembaliButton.addActionListener(new kembaliListener());
        
        frame.setLayout(null);

        ringkasanMahasiswaPanel.setBounds(0, 50, 500, 300);
        ringkasanMahasiswaPanel.add(titleLabel);
        ringkasanMahasiswaPanel.add(npmLbl);
        ringkasanMahasiswaPanel.add(npmBox);
        ringkasanMahasiswaPanel.add(lihatButton);
        ringkasanMahasiswaPanel.add(kembaliButton);
        

        frame.add(ringkasanMahasiswaPanel); 
    }

    class lihatListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String npmStr = "";
            long npm = 0;
            Mahasiswa mahasiswa = null;

            boolean fieldKosong = true;

            if (! Objects.isNull(npmBox.getSelectedItem())) {
                fieldKosong = false;
                npmStr = String.valueOf(npmBox.getSelectedItem());
                npm = Long.parseLong(npmStr);
                mahasiswa = getMahasiswa(npm);
            }

            if (!fieldKosong) {
                frame.getContentPane().removeAll();
                frame.repaint();

                new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah);
                frame.validate();

            } else {
                new OptionPane("Mohon isi seluruh field");
            }

        }
    }

    class kembaliListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();
            
            new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();
        }
    }

    public Long[] npmLister(ArrayList<Mahasiswa> e) {
        Long[] listNPM = new Long[e.size()];
        for (int i = 0; i < e.size(); i++) {
            listNPM[i] = e.get(i).getNpm();
        }

        return listNPM;
    }

    class OptionPane {  
        JFrame f;  
        OptionPane(String str){  
            f = new JFrame();  
            JOptionPane.showMessageDialog(f, str);  
        }  
    }

    // Uncomment method di bawah jika diperlukan
    private Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
