package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {

    private JFrame frame;

    private JLabel titleLabel = new JLabel("Detail Ringkasan Mahasiswa");
    private JLabel namaLabel = new JLabel();
    private JLabel npmLabel = new JLabel();
    private JLabel jurusanLabel = new JLabel();
    private JLabel daftarMataKuliahLbl = new JLabel("Daftar Mata Kuliah:");
    private JLabel sksLabel = new JLabel();
    private JLabel hasilPengecekanIRS = new JLabel("Hasil Pengecekan IRS:");
    
    private JButton selesaiButton = new JButton("Selesai");

    private JPanel homePanel = new JPanel(new GridLayout(14, 1, 5, 5));

    private ArrayList<Mahasiswa> daftarMahasiswa;
    private ArrayList<MataKuliah> daftarMataKuliah;

    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Detail Ringkasan Mahasiswa
        this.frame = frame;
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        namaLabel.setText(String.format("Nama: %s", mahasiswa.getNama()));
        namaLabel.setHorizontalAlignment(JLabel.CENTER);
        npmLabel.setText(String.format("NPM: %d", mahasiswa.getNpm()));
        npmLabel.setHorizontalAlignment(JLabel.CENTER);
        jurusanLabel.setText(String.format("Jurusan: %s", mahasiswa.getJurusan()));
        jurusanLabel.setHorizontalAlignment(JLabel.CENTER);
        daftarMataKuliahLbl.setHorizontalAlignment(JLabel.CENTER);
        
        sksLabel.setText(String.format("Total SKS: %d", mahasiswa.getTotalSKS()));
        sksLabel.setHorizontalAlignment(JLabel.CENTER);
        hasilPengecekanIRS.setHorizontalAlignment(JLabel.CENTER);

        frame.setLayout(null);
        
        selesaiButton.addActionListener(new selesaiListener());
        
        homePanel.setBounds(0, 50, 500, 500);
        homePanel.add(titleLabel);
        homePanel.add(namaLabel);
        homePanel.add(npmLabel);
        homePanel.add(jurusanLabel);
        homePanel.add(daftarMataKuliahLbl);
        printMatkul(mahasiswa);
        homePanel.add(sksLabel);
        homePanel.add(hasilPengecekanIRS);
        printMasalahIRS(mahasiswa);
        homePanel.add(selesaiButton);
        homePanel.setAutoscrolls(true);

        frame.getContentPane().add(homePanel);
        

    }

    class selesaiListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();

            new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            frame.validate();
        }
    }

    public void printMatkul(Mahasiswa mahasiswa) {
        String str = "";
        if (mahasiswa.getBanyakMatkul() > 0) {
            for (int i = 0; i < mahasiswa.getBanyakMatkul(); i++) {
                str = String.format("%d. %s", i+1, mahasiswa.getMataKuliah()[i].getNama());
                homePanel.add(new JLabel(str, JLabel.CENTER));
            }
        } else {
            homePanel.add(new JLabel("Belum ada mata kuliah yang diambil", JLabel.CENTER));
        }
    }

    public void printMasalahIRS(Mahasiswa mahasiswa) {
        String str = "";
        mahasiswa.cekIRS();

        if (mahasiswa.getBanyakMasalahIRS() > 0) {
            for (int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++) {
                str = String.format("%d. %s", i+1, mahasiswa.getMasalahIRS()[i]);
                homePanel.add(new JLabel(str, JLabel.CENTER));
            }
        } else {
            homePanel.add(new JLabel("IRS tidak bermasalah.", JLabel.CENTER));
        }
    }
}
