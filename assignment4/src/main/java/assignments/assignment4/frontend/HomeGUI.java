package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    private JFrame frame;
    private JButton tambahMahasiswaButton = new JButton("Tambah Mahasiswa");
    private JButton tambahMatkulButton = new JButton("Tambah Mata Kuliah");
    private JButton tambahIRSButton = new JButton("Tambah IRS");
    private JButton hapusIRSButton = new JButton("Hapus IRS");
    private JButton ringkasanMahasiswaButton = new JButton("Lihat Ringkasan Mahasiswa");
    private JButton ringkasanMatkulButton = new JButton("Lihat Ringkasan Mata Kuliah");

    private JPanel homePanel = new JPanel(new GridLayout(7, 1, 10, 10));

    private ArrayList<Mahasiswa> daftarMahasiswa;
    private ArrayList<MataKuliah> daftarMataKuliah;

    //Tiap button dan label dimasukkan ke dalam panel,
    //kemudian panel tersebut dimasukkan ke frame
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        this.frame = frame;
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        
        // TODO: Implementasikan Halaman Home
        frame.setLayout(null);
        
        tambahMahasiswaButton.addActionListener(new tambahMahasiswaListener());
        tambahMatkulButton.addActionListener(new tambahMatkulListener());
        tambahIRSButton.addActionListener(new tambahIRSListener());
        hapusIRSButton.addActionListener(new hapusIRSListener());
        ringkasanMahasiswaButton.addActionListener(new ringkasanMahasiswaListener());
        ringkasanMatkulButton.addActionListener(new ringkasanMatkulListener());
        
        homePanel.setBounds(0, 50, 500, 300);
        homePanel.add(titleLabel);
        homePanel.add(tambahMahasiswaButton);
        homePanel.add(tambahMatkulButton);
        homePanel.add(tambahIRSButton);
        homePanel.add(hapusIRSButton);
        homePanel.add(ringkasanMahasiswaButton);
        homePanel.add(ringkasanMatkulButton);

        frame.getContentPane().add(homePanel);

    }

    // listener untuk tombol tambah mahasiswa
    class tambahMahasiswaListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //menghapus content yang sedang ditampilkan kemudian memanggil
            //TambahMahasiswaGUI (menampilkan kontennya)
            frame.getContentPane().removeAll();
            frame.repaint();
            
            new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();
        }
    }

    class tambahMatkulListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();

            new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();
        }
    }

    class tambahIRSListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();

            new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();
        }
    }

    class hapusIRSListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();

            new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();
        }
    }

    class ringkasanMahasiswaListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();

            new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();
        }
    }

    class ringkasanMatkulListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            frame.getContentPane().removeAll();
            frame.repaint();

            new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah); 
            frame.validate();
        }
    }
}
